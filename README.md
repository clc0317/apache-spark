# apache-spark

Apache-spark log analysis system's shared notebooks and automated jobs.

Trying to make an s2i following this: https://blog.openshift.com/create-s2i-builder-image/?extIdCarryOver=true&sc_cid=701f2000001Css5AAC

oc create imagestream pyspark-bc-test-this
imagestream "pyspark-bc-test-this" created


 493  oc get templates
  494  oc describe template jupyter-notebook-cron
  495  oc get cronjob
  496  oc get jobs
  497  oc get jobs
  498  oc get templates
  499  oc get bc
  500  oc start-build pyspark-bc-test
  501  oc get bc
  502  oc create imagestream pyspark-bc-test-this

s2i notes - must pass in the notebook name - the desired notebook must get pulled by the container, and run by new-sparkmagic.py, so we'll need an env of the notebook name to pass to new-sparkmagic.py

Ok, this is the process.  The build config is per person/subdir, and can get by without the env (or they can be changed at runtime).  They should not have $NOTEBOOK.
The individual containers, at execution time, are passed $NETID if it doesn't already have one, and $NOTEBOOK. The execution time part is the next bit to figure out.

In other words - use the buildconfig of jupyter-s2i and make a new build of it per group/team (i.e. itso-build).
These images will get refreshed whenever something new is pushed to the appropriate repo subdir (check this part, but I thought this was the point).
Set up a run-once or run-cron for individual notebooks, with env $NETID (if needed) and $NOTEBOOK.

oc create imagestream image-by-user-bryn < somefilename.yaml

oc create imagestream bryn-imagestream - this makes the output of the cron template (maybe)
