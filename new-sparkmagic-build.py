#!/usr/local/bin/python3
# This script is part of the Openshift stream to image framework, in that it is used to plug in the user netID and ambari password to the sparkmagic config.json in order to build a pod with the correct credentials.  The things that are kept within the image are the vault approle, the template config, and this script itself.
# The script should be abstract enough that it takes the netID as fed in to it via openshift and runs as-is.  It then runs whatever notebooks it has, also uploaded at build time. The notebooks (ipynb) are uploaded either via volume or as config maps. This script is the only CMD, and it builds the sparkmagic config.jsonand executes the notebooks. In theory, the notebooks are written to email results to the user. Responsibility for email configuration should fall to the user.
# A build is triggered when a new notebook is added (where? gitlab?), and the user in question sets up a request for a build.  They then set a schedule for their notebooks to run.
# We should also have containers with stock scripts, although this is a tiny bit more complex - how do we get the user's email address in to the notebook?
# The next problem to solve is getting the netID variable from openshift into the image to use with this script. After that, how to mount/read in the notebooks.
# If we get the user to pass in the notebook name, after putting it in from gitlab, we can get this container/script to run only that notebook.

import hvac
import json
import yaml
import os
import requests
import sys
import subprocess
from subprocess import call
import pwd
from pwd import getpwuid
from shutil import copyfile
import argparse


# We want to pull in the username as an environmental variable from docker/openshift, not from the script.  Convince openshift to pass an environmental variable of USER (or whatever). Maybe have an env. of NETID? This might be set this way on the command line: oc set env bc/apache-spark NETID=$USER
scoped=os.environ['NETID']

#print ("scoped = %s" % scoped)
try:
	splitscoped = scoped.split('@')
	myname = splitscoped[0]
	#print ("myname = %s" % myname)
except:
	myname = scoped
	#print ("myname = %s" % myname)

# Now we need to tell the s2i the name of the notebook we want to run, so it can run only that one even if it pulls in more of them from the distro, or if they were inadvertently left in the container, or if we re-use the same pod but don't want to run every nbconvert.ipynb in here
notebook=os.environ['NOTEBOOK']
notebookpath = "/home/jovyan/%s" % (notebook)

###### in the prod version, this template is in /home/jovyan, but for testing, it's in the Mac's local dir.  It is also part of the image, not created by the dockerfile build.
#magic_config='/Users/bryn/Dcuments/work/spark/workspace/config-template.json'
magic_config='/home/jovyan/config-template.json'

###### Vault get the args for reading approle information out of ~/.vault_role.yaml which is part of the image, and not created by the dockerfile build.
def parse_args():
    parser = argparse.ArgumentParser(
        description=(
          "Refresh vault tokens"
        )
    )
## Mac python wants the whole path, on porting use the ~ one
#    default_token_file = os.path.expanduser("/Users/bryn/.vault_role.yaml")
    default_token_file = os.path.expanduser("~/.vault_role.yaml")
    parser.add_argument('-i', '--vault-token-file',
                        default=default_token_file,
                        type=argparse.FileType('r'))
    return parser.parse_args()
###### End of reading vault_role.yaml

###### Get the approle from vault

def get_approle_token(vault_info):
    # Log in to the approle
    login = requests.post(
        "%s/v1/auth/approle/login" % vault_info['vault']['vault_addr'],
        json={
            'role_id': vault_info['vault']['role_id'],
            'secret_id': vault_info['vault']['secret_id']
        }
    )

    try:
        token = login.json()['auth']['client_token']
    except Exception:
        sys.stderr.write(
            "Could not get client token, please check your credentials\n")
        sys.exit(2)

    return token
###### End approle

###### Start get_pw
def get_pw(vault_info,current_token,myname):
    vault_url = (vault_info['vault']['vault_addr'])
    whole_request = ("%s/v1/secret/apache-spark/%s" % (vault_url, myname))

    json_header={ 'X-Vault-Token': current_token }
    passwd = requests.get(whole_request,headers=json_header)
    pw_string = (passwd.text)
    parsed_passwd = json.loads(pw_string)
#    print(parsed_passwd['data']['password'])
    spark_pw = parsed_passwd['data']['password']
    return spark_pw
###### End get_pw




# The config template for the sparkmagic json is uploaded to the container at build time.
#with open('/srv/scripts/config-template.json', 'r') as fp:
#with open('/home/bryn/repos/apache-spark/config-template.json', 'r') as fp:
with open('/home/jovyan/config-template.json', 'r') as fp:
	obj = json.load(fp)


def makesparkmagic(myname,sparkmagic_pw):
#	print (obj)
	kpc = obj["kernel_python_credentials"]
#	print (kpc)
	ksc=obj["kernel_scala_credentials"]
	ksc["username"]=myname
	ksc["password"]=sparkmagic_pw
#	print (ksc)
	kpc = obj["kernel_python_credentials"]
	kpc["username"]=myname
	kpc["password"]=sparkmagic_pw
#	print (kpc)
	magicconfig="/home/jovyan/.sparkmagic/config.json" 
###	magicconfig="/Users/%s/.sparkmagic/config.json" % (myname)
	session_configs=obj["session_configs"]
	session_configs["proxyUser"]=myname
	with open(magicconfig, 'w') as fp:
		json.dump(obj, fp, indent=4)


def main():
# Get some config information
    args = parse_args()
# Read the vault approle
    vault_info_contents = yaml.load(args.vault_token_file)
# Use the vault approle to log in to vault and get a token
    vault_info = {'vault': vault_info_contents}
    current_token = get_approle_token(vault_info)
# Use the token to get the password from vault for the user we got from the environmental variable (which we don't know how to do yet)
    sparkmagic_pw = get_pw(vault_info,current_token,myname)
#    print ("sparkmagic_pw in main = %s" % sparkmagic_pw)
# Finally, we build the sparkmagic config.json so we can run the notebooks
    makesparkmagic(myname,sparkmagic_pw)
### Next, we call any notebooks that happen to be in the home dir.  
    #print ("notebookpath = %s" % (notebookpath))
    subprocess.check_output(["/opt/conda/bin/jupyter","nbconvert","--to","notebook","--execute",notebookpath])


main()
