# You should definitely use tags and not rely on 'latest'
# because 'latest' can change out from under you
FROM jupyter/all-spark-notebook:latest

# Always add a maintainer label so folks know who to talk
# to if they have questions
LABEL maintainer "Bryn Smith <bryn.smith@duke.edu>"

# This label is REQUIRED for S2I to know where your scripts are
LABEL io.openshift.s2i.scripts-url="image:///usr/libexec/s2i"

USER 0

RUN mkdir -p /srv/spark/python
COPY s2i/bin/ /usr/libexec/s2i
COPY jupyterhelpers/DukeGraphFunctions.py /srv/spark/python

USER 1000

RUN /opt/conda/bin/pip install sparkmagic hvac
RUN mkdir /home/jovyan/.sparkmagic

RUN /opt/conda/bin/jupyter-kernelspec install \
      /opt/conda/lib/python3.6/site-packages/sparkmagic/kernels/pysparkkernel --user
RUN /opt/conda/bin/jupyter-kernelspec install \
      /opt/conda/lib/python3.6/site-packages/sparkmagic/kernels/sparkkernel --user

COPY config-template.json /home/jovyan/
COPY new-sparkmagic-build.py /home/jovyan/
COPY *.ipynb /home/jovyan/

# Builder images don't have commands - they never run that way
# Their child images do, and with s2i, that's specified in the "run" script
# Looks like you already did this, but we can remove the CMD here
# CMD NONE!

# first-go works up to this point
#ENV NETID=bryn@duke.edu
#ENV NOTEBOOK=send-email.ipynb
#COPY ./send-email.ipynb /home/jovyan
# everything works!
