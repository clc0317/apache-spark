#!/usr/bin/python3
# This script is part of the Openshift stream to image framework, in that it is used to plug in the user netID and ambari password to the sparkmagic config.json in order to build a pod with the correct credentials.
# The problem at the moment is that the passwords to connect to spark are created by spark on apache-spark-01 and put in an xml file, so until there is a way to get those out of there, they'll have to be added by hand somewhere. These passwords are stored in vault, though, so eventually we'll have to figure out if we can update the passwords on the fly - i.e. if you're running a job and your ambari pw changes, do you lose your connection? Or do you get to finish that job before you need to restart the kernel?

import json
import yaml
import os
from shutil import copyfile
import argparse


myname=os.environ['USER']

print ("hi")

###### Vault get the args out of ~/.vault_role.yaml
def parse_args():
    print ("*****")
    print ("parse_args module here ")
    parser = argparse.ArgumentParser(
        description=(
          "Refresh vault tokens"
        )
    )
## Mac python wants the whole path, on porting use the ~ one
#    default_token_file = os.path.expanduser("/Users/bryn/.vault_role.yaml")
    default_token_file = os.path.expanduser("~/.vault_role.yaml")
    print (default_token_file)
    print
    parser.add_argument('-i', '--vault-token-file',
                        default=default_token_file,
                        type=argparse.FileType('r'))
    print ("end parse_args module ")
    print ("*****")
    return parser.parse_args()
###### End of reading vault_role.yaml


def main():
    print ("*****")
    print ("main module")
    args = parse_args()
    print ("args = %s" % args)
    print
    print ("end main module")
    print ("*****")

main()
